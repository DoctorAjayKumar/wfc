% @doc
% A library of
%
% - all 4 truth tables with arity 1
% - all 16 truth tables with arity 2
%
% with all the common aliases (xor, times, plus, etc)
%
% A truth table is a function from 0|1 to 0|1. That's it.
%
% In general, there are 2^(2^A) truth tables with arity A. In
% particulary, there are 256 truth tables with arity 3, so those are
% not exported. Instead, there is a function to generate a ``truth
% table'' (which takes as an argument a list of bits).
%
% This is a dumb data structure because a truth table that a user
% inputs has to contain an arbitrary number of arguments. And there
% should be only one "truth table" data structure
-module(wfc_tt).

-export_type([
    bit/0
]).

% arity 1 truth tables
-export([
    tt_00/1,
    tt_01/1,
    tt_10/1,
    tt_11/1,
    tt_id/1,
    tt_not/1
]).
% arity 2 truth tables
-export([
    tt_0000/2,
    tt_0001/2,
    tt_0010/2,
    tt_0011/2,
    tt_0100/2,
    tt_0101/2,
    tt_0110/2,
    tt_0111/2,
    tt_1000/2,
    tt_1001/2,
    tt_1010/2,
    tt_1011/2,
    tt_1100/2,
    tt_1101/2,
    tt_1110/2,
    tt_1111/2
]).


-type bit() :: 0 | 1.



%%% API: arity 1 truth tables

-spec tt_00(bit()) -> bit().

tt_00(0) -> 0;
tt_00(1) -> 0.



-spec tt_01(bit()) -> bit().

tt_01(0) -> 0;
tt_01(1) -> 1.



-spec tt_10(bit()) -> bit().

tt_10(0) -> 1;
tt_10(1) -> 0.



-spec tt_11(bit()) -> bit().

tt_11(0) -> 1;
tt_11(1) -> 1.



-spec tt_id(bit()) -> bit().
% @doc
% Truth table corresponding to identity function

tt_id(X) ->
    tt_01(X).



-spec tt_not(bit()) -> bit().
% @doc
% Truth table corresponding to logical-not function (flips the bit)

tt_not(X) ->
    tt_10(X).



%%% API: arity 2 functions

-spec tt_0000(bit(), bit()) -> bit().

tt_0000(0, 0) -> 0;
tt_0000(0, 1) -> 0;
tt_0000(1, 0) -> 0;
tt_0000(1, 1) -> 0.



-spec tt_0001(bit(), bit()) -> bit().

tt_0001(0, 0) -> 0;
tt_0001(0, 1) -> 0;
tt_0001(1, 0) -> 0;
tt_0001(1, 1) -> 1.



-spec tt_0010(bit(), bit()) -> bit().

tt_0010(0, 0) -> 0;
tt_0010(0, 1) -> 0;
tt_0010(1, 0) -> 1;
tt_0010(1, 1) -> 0.



-spec tt_0011(bit(), bit()) -> bit().

tt_0011(0, 0) -> 0;
tt_0011(0, 1) -> 0;
tt_0011(1, 0) -> 1;
tt_0011(1, 1) -> 1.



-spec tt_0100(bit(), bit()) -> bit().

tt_0100(0, 0) -> 0;
tt_0100(0, 1) -> 1;
tt_0100(1, 0) -> 0;
tt_0100(1, 1) -> 0.



-spec tt_0101(bit(), bit()) -> bit().

tt_0101(0, 0) -> 0;
tt_0101(0, 1) -> 1;
tt_0101(1, 0) -> 0;
tt_0101(1, 1) -> 1.



-spec tt_0110(bit(), bit()) -> bit().

tt_0110(0, 0) -> 0;
tt_0110(0, 1) -> 1;
tt_0110(1, 0) -> 1;
tt_0110(1, 1) -> 0.



-spec tt_0111(bit(), bit()) -> bit().

tt_0111(0, 0) -> 0;
tt_0111(0, 1) -> 1;
tt_0111(1, 0) -> 1;
tt_0111(1, 1) -> 1.



-spec tt_1000(bit(), bit()) -> bit().

tt_1000(0, 0) -> 1;
tt_1000(0, 1) -> 0;
tt_1000(1, 0) -> 0;
tt_1000(1, 1) -> 0.



-spec tt_1001(bit(), bit()) -> bit().

tt_1001(0, 0) -> 1;
tt_1001(0, 1) -> 0;
tt_1001(1, 0) -> 0;
tt_1001(1, 1) -> 1.



-spec tt_1010(bit(), bit()) -> bit().

tt_1010(0, 0) -> 1;
tt_1010(0, 1) -> 0;
tt_1010(1, 0) -> 1;
tt_1010(1, 1) -> 0.



-spec tt_1011(bit(), bit()) -> bit().

tt_1011(0, 0) -> 1;
tt_1011(0, 1) -> 0;
tt_1011(1, 0) -> 1;
tt_1011(1, 1) -> 1.



-spec tt_1100(bit(), bit()) -> bit().

tt_1100(0, 0) -> 1;
tt_1100(0, 1) -> 1;
tt_1100(1, 0) -> 0;
tt_1100(1, 1) -> 0.



-spec tt_1101(bit(), bit()) -> bit().

tt_1101(0, 0) -> 1;
tt_1101(0, 1) -> 1;
tt_1101(1, 0) -> 0;
tt_1101(1, 1) -> 1.



-spec tt_1110(bit(), bit()) -> bit().

tt_1110(0, 0) -> 1;
tt_1110(0, 1) -> 1;
tt_1110(1, 0) -> 1;
tt_1110(1, 1) -> 0.



-spec tt_1111(bit(), bit()) -> bit().

tt_1111(0, 0) -> 1;
tt_1111(0, 1) -> 1;
tt_1111(1, 0) -> 1;
tt_1111(1, 1) -> 1.
