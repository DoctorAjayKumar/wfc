# TODONE

- gutted the internals of wfc to no longer store variables as atoms
  (uses binaries instead)
- add version headers

# TODONEISH

- redo the porcelain

# TODO

- library of common truth tables
- way to input arbitrary truth tables
- truth table to sentence given variable names
- sentence to truth table
- way to list every glyph that appears in a sentence
- wfcl: pretty print truth table

- needs to have arbitrary truth tables
- needs to be able to pretty print a truth table

- define a truth table as a function that takes 0s or 1s and outputs
  either a 0 or a 1

  suffix `_tt`

- a sentence function is some trickery over the truth table that
  allows inputting sentences

  suffix `_sf`

- a sentence macro is a function that takes a list of sentences and
  does some fold or something over them, mostly for syntax
  convenience; example would be something like `(and a b c)`

  suffix `_sm`

- maybe: need to split the lisp server into its own thing

  - needs state
  - that way it can have patterns
  - and the web interface can remember your history by storing some
    session cookie

# wfc pseudocode

```
; core forms
(+)
(*)
(|)
(=)
(lambda)
(case)

(= Xor_f (lambda (X Y) (+ X Y)))
(= Xor_m
    (lambda (Items)
        (case Items
            ((|)        0)
            ((| X Xs)   (+ X (Xor_m Xs))))

(defmacro xor_m
    ((|)      0)
    ((| X Xs) (xor_f X (xor_m Xs)))
```
