-module(wfc_listfuns).
-vsn("1.0.0").

-compile(export_all).


land(List) -> wfc:prod(List).
liff([A, B])       -> wfc:wiff(A, B).
limpliedby([A, B]) -> wfc:wimpliedby(A, B).
limplies([A, B])   -> wfc:wimplies(A, B).

lior(List) ->
    lists:foldl(fun wfc:wior/2, zero(), List).

lnot([X])  -> wfc:wnot(X).
lxor(List) -> wfc:sum(List).

zero() ->
    wfc:sum([]).

one() ->
    wfc:prod([]).
