% connectives, just hardcoded
wxor(A, B) ->
    sum([A, B]).

wand(A, B) ->
    prod([A, B]).

wnot(A) ->
    sum([1, A]).

wrc(A, B) ->
    sum([A, prod([A, B])]).

wior(A, B) ->
    sum([A, B, prod([A, B])]).

wimplies(A, B) ->
    sum([1, A, prod([A, B])]).

wimpliedby(A, B) ->
    sum([1, B, prod([A, B])]).

wiff(A, B) ->
    sum([1, A, B]).


%% connectives of set theory:
wcap(A, B) -> wand(A, B).
wcup(A, B) -> wior(A, B).
