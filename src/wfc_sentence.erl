%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% sentences are a set of words
%
% with a word, multiplication is implied
% with a sentence, summation is implied
-module(wfc_sentence).
-vsn("1.0.0").

-export_type([
    sentence/0
]).
-export([
    zero/0,
    is_zero/1,
    one/0,
    is_one/1,
    is_valid_sentence/1,
    from_word/1,
    from_list/1,
    to_list/1,
    plus/1,
    plus/2,
    times/1,
    times/2,
    eval/2,
    pf/1,
    pp/1
]).


-type map01()    :: wfc_word:map01().
-type sentence() :: {s, sets:set(word())}.
%-type set()      :: sets:set().
-type word()     :: wfc_word:word().

%%% API

-spec zero() -> sentence().
% @doc
% the zero-sentence is the empty sentence

zero() ->
    {s, sets:new()}.



-spec is_zero(term()) -> boolean().
% @doc a sentence is zero if it is empty

is_zero(Sent) ->
    Sent =:= zero().



-spec one() -> sentence().
% @doc
% The one-sentence is the sentence containing only the empty word

one() ->
    WordOne = wfc_word:one(),
    from_list([WordOne]).



-spec is_one(term()) -> boolean().
% @doc tests if argument equals one().

is_one(X) ->
    X =:= one().



-spec is_valid_sentence(term()) -> boolean().
% @doc
% is valid if
%
%   - is a tagged tuple {s, Set}
%   - set contains only valid words

is_valid_sentence({s, Set}) ->
    Pred =
        fun(Elem, Accum) ->
            Accum andalso wfc_word:is_valid_word(Elem)
        end,
    sets:fold(Pred, true, Set);
is_valid_sentence(_) ->
    false.



-spec from_word(word()) -> sentence().
% @doc
% Singleton word

from_word(W) ->
    true = wfc_word:is_valid_word(W),
    SentenceSet = sets:from_list([W]),
    Sentence = {s, SentenceSet},
    true = is_valid_sentence(Sentence),
    Sentence.



-spec from_list([word()]) -> sentence().
% @doc
% from a list of WORDS, not a list of atoms

from_list(Words) ->
    Summands = [from_word(W) || W <- Words],
    plus(Summands).



-spec to_list(sentence()) -> [word()].
% @doc
% convert a SENTENCE to a list of WORDS, NOT a list of atoms
% and sorts the list of words

to_list({s, Set}) ->
    lists:sort(sets:to_list(Set)).



-spec plus([sentence()]) -> sentence().
% @doc
% Add a list of sentences

plus(Sentences) ->
    plus_acc(Sentences, zero()).

plus_acc([], FinalAcc) ->
    FinalAcc;
plus_acc([S | Rest], Acc) ->
    NewAcc = plus(S, Acc),
    plus_acc(Rest, NewAcc).



-spec plus(sentence(), sentence()) -> sentence().
% @doc
% addition of two sentences is just the symmetric difference

plus({s, L}, {s, R}) ->
    SetLR = set_symdiff(L, R),
    Sent = {s, SetLR},
    true = is_valid_sentence(Sent),
    Sent.

set_symdiff(X, Y) ->
    Cup = sets:union(X, Y),
    Cap = sets:intersection(X, Y),
    sets:subtract(Cup, Cap).



-spec times([sentence()]) -> sentence().
% @doc
% add a list of sentences
times(Sentences) ->
    times_acc(Sentences, one()).

times_acc([], FinalAcc) ->
    FinalAcc;
times_acc([S | Rest], Acc) ->
    NewAcc = times(S, Acc),
    times_acc(Rest, NewAcc).



-spec times(sentence(), sentence()) -> sentence().
% @doc
% Multiply two sentences

times(SL, SR) ->
    sxs(SL, SR).



-spec eval(sentence(), map01()) -> 0 | 1.
% @doc Evaluate a sentence against a map atom -> 0 | 1

eval(Sent = {s, WordSet}, Map) ->
    true = is_valid_sentence(Sent),
    Fold =
        fun(Word, Accum) ->
            % summation is implied in a sentence
            % first, we get the value of the word
            Val01 = wfc_word:eval(Word, Map),
            % then take the value of that word, add it to the
            % accumulator
            NewAccum = lxor(Val01, Accum),
            % return new accumulator
            NewAccum
        end,
    sets:fold(Fold, 0, WordSet).

lxor(0, 0) -> 0;
lxor(0, 1) -> 1;
lxor(1, 0) -> 1;
lxor(1, 1) -> 0.



-spec pp(sentence()) -> ok.
% @doc pp = pretty print

pp(X) ->
    io:format("~ts~n", [pf(X)]).



-spec pf(sentence()) -> iolist().
% @doc
% pf = pretty format
%
% Formats as
%
%   (+ Word1 Word2 ...)

pf(Sent) ->
    Words = to_list(Sent),
    Strs = pf_words(Words, []),
    ["(+", Strs, ")"].

pf_words([], Accum) ->
    Accum;
pf_words([W | Ws], Accum) ->
    WordStr = wfc_word:pf(W),
    NewAccum = [Accum, " ", WordStr],
    pf_words(Ws, NewAccum).



%%% PRIVATE INTERNAL

-spec sxs(sentence(), sentence()) -> sentence().
% (A + B)(C + D)
sxs(SentL, SentR) ->
    true = is_valid_sentence(SentL),
    true = is_valid_sentence(SentR),
    % fold over list of words in the left sentence, multiply through
    % by right sentence, take sum of result
    % ie
    % split up the left word into
    %
    %   (A)(C + D) + (B)(C + D)
    %
    % That word times sentence operation is done in wxs/2
    Fold =
        fun(WordL, AccumSentence) ->
            Summand = wxs(WordL, SentR),
            NewAccum = plus(Summand, AccumSentence),
            NewAccum
        end,
    {s, SetL} = SentL,
    Result = sets:fold(Fold, zero(), SetL),
    true = is_valid_sentence(Result),
    Result.

-spec wxs(word(), sentence()) -> sentence().
% @private
% A(C + D)
%
% split this into AC + AD
% each of those operations are done in wxw
% @end
wxs(WordL, SentR) ->
    Fold =
        fun(WordR, AccumSentence) ->
            Summand = wxw(WordL, WordR),
            NewAccum = plus(Summand, AccumSentence),
            NewAccum
        end,
    {s, SetR} = SentR,
    Result = sets:fold(Fold, zero(), SetR),
    Result.

-spec wxw(word(), word()) -> sentence().
% @private
% word times word
% A*C
% Important to note that this returns a *SENTENCE*
% @end
wxw(WordL, WordR) ->
    NewWord = wfc_word:times(WordL, WordR),
    Sent = from_list([NewWord]),
    Sent.
