- Code review sentence
- Code review top level wfc
- add common combinators
- way to input arbitrary truth table
- add pretty-print truth table


wfcl:

- better parser (multi-pass with handwritten tokenizer, don't try to
  be l333t and use leex, just do it by hand).

- If possible, but is not necessary: have variables and the = combinator
- documentation (so the (help X) function

ophttpd:

- a router behavior:
    - function `can_you_handle_this`
    - given a request, return either
        - `{yes, Handler}`
        - `no`

- handler behavior

    - function `handle`
        - given a request, return a response

- don't worry about HTTPS for now, let nginx handle that, hide behind
  a NGINX reverse proxy, that's fine
