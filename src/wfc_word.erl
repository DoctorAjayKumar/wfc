%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% a word is the smallest unit in a reduced sum. In for instance
% 1 + a + ab, the words are 1, a, and ab, which are represented as
% the sets {}, {a}, and {a, b}, respectively.
%
% - a word is a tuple {w, SetOfWFChars}
%   - the empty set means 1
%
% - a wfchar is a tuple {c, Binary}
%   - if you wish to use pf/1, the binary must be string-formattable
%
% in WF algebra, anything times itself equals itself, therefore we
% don't need to keep track of exponents. That is why the set
% representation makes sense.
%
% with a word, multiplication is implied
% with a sentence, summation is implied
-module(wfc_word).
-vsn("1.0.0").

-export_type([
    map01/0,
    wfchar/0,
    word/0
]).
-export([
    one/0,
    is_one/1,
    is_valid_word/1,
    from_binary/1,
    from_list/1,
    to_list/1,
    times/1,
    times/2,
    eval/2,
    pf/1,
    pp/1
]).

-type wfchar() :: binary().
-type map01()  :: #{wfchar() := 0 | 1}.
-type word()   :: {w, sets:set(wfchar())}.


%%% API


-spec one() -> word().
% @doc The word corresponding to the concept "1"; it is a tagged
% tuple of {w, EmptySet}.

one() ->
    {w, sets:new()}.



-spec is_one(term()) -> boolean().
% @doc a word is one if it {w, EmptySet}.

is_one(Word) ->
    Word =:= one().



-spec is_valid_word(term()) -> boolean().
% @doc
% a word is valid if exactly one of these conditions are true:
%
%   - is empty
%   - contains only valid wfchars
%
% return false on anything failing to pattern match {w, Set}

is_valid_word({w, Set}) ->
    Pred =
        fun(Element, Accumulator) ->
            Accumulator andalso is_valid_char(Element)
        end,
    sets:fold(Pred, true, Set);
is_valid_word(_) ->
    false.

is_valid_char(X) ->
    is_binary(X).



-spec from_binary(binary()) -> word().
% @doc
% Convert a binary into a word

from_binary(Glyph) when is_binary(Glyph) ->
    Set = sets:from_list([Glyph]),
    Word = {w, Set},
    true = is_valid_word(Word),
    Word.



-spec from_list([binary()]) -> word().
% @doc
% Given a list of binaries, take their "product" and put it into a
% word.

from_list(Binaries) ->
    % works by putting eacch glyph into its own word and taking the
    % product
    %
    % sum operands = summands
    % product operands = productands
    Productands = [from_binary(B) || B <- Binaries],
    ResultWord = times(Productands),
    true = is_valid_word(ResultWord),
    ResultWord.



-spec to_list(word()) -> [wfchar()].
% @doc
% pull out the set in the tagged tuple, convert it to a list, and
% return the sorted list
% @end

to_list({w, Set}) ->
    Chars = sets:to_list(Set),
    lists:sort(Chars).



-spec times([word()]) -> word().
% @doc product of a list of words

times(Words) ->
    times_acc(Words, one()).


times_acc([], FinalAcc) ->
    FinalAcc;
times_acc([W | Ws], Acc) ->
    NewAcc = times(W, Acc),
    times_acc(Ws, NewAcc).



-spec times(word(), word()) -> word().
% @doc
% Multiply two words. This amounts to just taking the union of the
% characters contained in the words
% @end

times({w, L}, {w, R}) ->
    % take the unions of the things it contains
    LR = sets:union(L, R),
    Word = {w, LR},
    true = is_valid_word(Word),
    Word.



-spec eval(word(), map01()) -> 0 | 1.
% @doc
% evaluate a word; given a word, and a map that maps every atom to 1
% or 0, take the product of all the atoms in the word

eval({w, Set}, Map) ->
    Folder =
        fun(Char, Accum) ->
            Val01 = maps:get(Char, Map),
            NewAccum = Val01 * Accum,
            NewAccum
        end,
    sets:fold(Folder, 1, Set).



-spec pp(word()) -> ok.
% @doc pretty print a word (wraps an io:format/2 call around pf/1).

pp(Word) ->
    io:format("~ts~n", [pf(Word)]).



-spec pf(word()) -> iolist().
% @doc
% returns iolist
%
% "(*)"         if word is 1
% "(* a b c)"   if word is the set containing {a,b,c}

pf(Word) ->
    true = is_valid_word(Word),
    Chars = to_list(Word),
    Strs = pf_wfchars(Chars, []),
    ["(*", Strs, ")"].

pf_wfchars([], Accum) ->
    Accum;
pf_wfchars([Glyph | Rest], Accum) when is_binary(Glyph) ->
    GlyphStr = io_lib:format("~s", [Glyph]),
    NewAccum = [Accum, " ", GlyphStr],
    pf_wfchars(Rest, NewAccum).
