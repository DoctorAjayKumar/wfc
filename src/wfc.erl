%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
-module(wfc).
-vsn("1.0.0").

-export_type([sentence/0]).

% API: basics
-export([
    sum/1,
    prod/1,
    eval/2,
    pf/1,
    pp/1
]).
% API: truth tables

%-export([pp/1, pf/1, sum/1, prod/1, eval/2]).
%-export([wxor/2, wand/2, wnot/1, wrc/2, wior/2,
%         wimplies/2, wimpliedby/2, wiff/2]).
%-export([wcap/2, wcup/2]).

-type map01()    :: wfc_word:map01().
-type sentence() :: wfc_sentence:sentence().
-type word()     :: wfc_word:word().
-type glyph()    :: integer() | atom() | binary().
-type wf()       :: glyph() | word() | sentence().
-type wfs()      :: [wf() | wfs()].


%%% API: basics

-spec sum(wfs())  -> sentence().

sum(Items) ->
    sum(Items, wfc_sentence:zero()).

sum([], Accum) ->
    Accum;
sum([Int | Rest], Accum) when is_integer(Int) ->
    Sent1 = integer_to_sentence(Int),
    NewAccum = wfc_sentence:plus(Sent1, Accum),
    sum(Rest, NewAccum);
sum([Atom | Rest], Accum) when is_atom(Atom) ->
    Sent = atom_to_sentence(Atom),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([Binary | Rest], Accum) when is_binary(Binary) ->
    Sent = binary_to_sentence(Binary),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([List | Rest], Accum) when is_list(List) ->
    Sent = sum(List),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([Word = {w,_} | Rest], Accum) ->
    true = wfc_word:is_valid_word(Word),
    Sent = wfc_sentence:from_word(Word),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([Sent = {s,_} | Rest], Accum) ->
    true = wfc_sentence:is_valid_sentence(Sent),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum).



-spec prod(wfs()) -> sentence().

prod(Items) ->
    prod(Items, one()).


prod([], Accum) ->
    Accum;
prod([Int | Rest], Accum) when is_integer(Int) ->
    Sent1 = integer_to_sentence(Int),
    NewAccum = wfc_sentence:times(Sent1, Accum),
    prod(Rest, NewAccum);
prod([Atom | Rest], Accum) when is_atom(Atom) ->
    Sent = atom_to_sentence(Atom),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([Binary | Rest], Accum) when is_binary(Binary) ->
    Sent = binary_to_sentence(Binary),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([List | Rest], Accum) when is_list(List) ->
    Sent = prod(List),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([Word = {w,_} | Rest], Accum) ->
    true = wfc_word:is_valid_word(Word),
    Sent = wfc_sentence:from_word(Word),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([Sent = {s,_} | Rest], Accum) ->
    true = wfc_sentence:is_valid_sentence(Sent),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum).



-spec eval(sentence(), map01()) -> 0 | 1.

eval(Sentence, Map) ->
    wfc_sentence:eval(Sentence, Map).



-spec pp(sentence()) -> ok.

pp(Sentence) ->
    io:format("~ts~n", [pf(Sentence)]).



-spec pf(sentence()) -> iolist().

pf(Sentence) ->
    unicode:characters_to_list(wfc_sentence:pf(Sentence)).



%%% HELPERS

integer_to_sentence(0) ->
    wfc_sentence:zero();
integer_to_sentence(1) ->
    wfc_sentence:one();
integer_to_sentence(N) when is_integer(N) ->
    integer_to_sentence(mod2(N)).

mod2(N) when is_integer(N), 0 =< N ->
    N rem 2;
mod2(N) when is_integer(N), N < 0 ->
    mod2(-1 * N).

atom_to_sentence(Atom) when is_atom(Atom) ->
    B = atom_to_binary(Atom),
    W = wfc_word:from_binary(B),
    wfc_sentence:from_word(W).

binary_to_sentence(Bin) when is_binary(Bin) ->
    W = wfc_word:from_binary(Bin),
    wfc_sentence:from_word(W).


one() ->
    wfc_sentence:one().
