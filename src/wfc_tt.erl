% @doc
% A library of
%
% - all 4 truth tables with arity 1
% - all 16 truth tables with arity 2
%
% with all the common aliases (xor, times, plus, etc)
%
% A truth table is a function from 0|1 to 0|1. That's it.
%
% In general, there are 2^(2^A) truth tables with arity A.
-module(wfc_tt).

-export_type([
    bit/0
]).

% arity 1 truth tables
-export([
    tt1/2,
    tt_id/1,
    tt_not/1
]).
% arity 2 truth tables
-export([
    tt2/3
]).


-type bit() :: 0 | 1.


-spec tt1(2#00..2#11, bit()) -> bit().

% the 00 truth table
tt1(2#00, 0) -> 0;
tt1(2#00, 1) -> 0.
% the 01 truth table
tt1(2#01, 0) -> 0;
tt1(2#01, 1) -> 1;
% the 10 truth table
tt1(2#10, 0) -> 1;
tt1(2#10, 1) -> 0;
% the 10 truth table
tt1(2#11, 0) -> 1;
tt1(2#11, 1) -> 1.



-spec tt_id(bit()) -> bit().
% @doc
% Truth table corresponding to identity function

tt_id(X) ->
    tt(2#01, X).



-spec tt_not(bit()) -> bit().
% @doc
% Truth table corresponding to logical-not function (flips the bit)

tt_not(X) ->
    tt(2#10, X).



%%% API: arity 2 functions

-spec tt2(2#0000..2#1111, bit(), bit()) -> bit().

tt2(2#0000, 0, 0) -> 0;
tt2(2#0000, 0, 1) -> 0;
tt2(2#0000, 1, 0) -> 0;
tt2(2#0000, 1, 1) -> 0;

tt2(2#0001, 0, 0) -> 0;
tt2(2#0001, 0, 1) -> 0;
tt2(2#0001, 1, 0) -> 0;
tt2(2#0001, 1, 1) -> 1;

tt2(2#0010, 0, 0) -> 0;
tt2(2#0010, 0, 1) -> 0;
tt2(2#0010, 1, 0) -> 1;
tt2(2#0010, 1, 1) -> 0;

tt2(2#0011, 0, 0) -> 0;
tt2(2#0011, 0, 1) -> 0;
tt2(2#0011, 1, 0) -> 1;
tt2(2#0011, 1, 1) -> 1;

tt2(2#0100, 0, 0) -> 0;
tt2(2#0100, 0, 1) -> 1;
tt2(2#0100, 1, 0) -> 0;
tt2(2#0100, 1, 1) -> 0;

tt2(2#0101, 0, 0) -> 0;
tt2(2#0101, 0, 1) -> 1;
tt2(2#0101, 1, 0) -> 0;
tt2(2#0101, 1, 1) -> 1;

tt2(2#0110, 0, 0) -> 0;
tt2(2#0110, 0, 1) -> 1;
tt2(2#0110, 1, 0) -> 1;
tt2(2#0110, 1, 1) -> 0;

tt2(2#0111, 0, 0) -> 0;
tt2(2#0111, 0, 1) -> 1;
tt2(2#0111, 1, 0) -> 1;
tt2(2#0111, 1, 1) -> 1;

tt2(2#1000, 0, 0) -> 1;
tt2(2#1000, 0, 1) -> 0;
tt2(2#1000, 1, 0) -> 0;
tt2(2#1000, 1, 1) -> 0;

tt2(2#1001, 0, 0) -> 1;
tt2(2#1001, 0, 1) -> 0;
tt2(2#1001, 1, 0) -> 0;
tt2(2#1001, 1, 1) -> 1;

tt2(2#1010, 0, 0) -> 1;
tt2(2#1010, 0, 1) -> 0;
tt2(2#1010, 1, 0) -> 1;
tt2(2#1010, 1, 1) -> 0;

tt2(2#1011, 0, 0) -> 1;
tt2(2#1011, 0, 1) -> 0;
tt2(2#1011, 1, 0) -> 1;
tt2(2#1011, 1, 1) -> 1;

tt2(2#1100, 0, 0) -> 1;
tt2(2#1100, 0, 1) -> 1;
tt2(2#1100, 1, 0) -> 0;
tt2(2#1100, 1, 1) -> 0;

tt2(2#1101, 0, 0) -> 1;
tt2(2#1101, 0, 1) -> 1;
tt2(2#1101, 1, 0) -> 0;
tt2(2#1101, 1, 1) -> 1;

tt2(2#1110, 0, 0) -> 1;
tt2(2#1110, 0, 1) -> 1;
tt2(2#1110, 1, 0) -> 1;
tt2(2#1110, 1, 1) -> 0;

tt2(2#1111, 0, 0) -> 1;
tt2(2#1111, 0, 1) -> 1;
tt2(2#1111, 1, 0) -> 1;
tt2(2#1111, 1, 1) -> 1.
