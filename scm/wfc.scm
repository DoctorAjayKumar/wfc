;; BUG! CANNOT DISTINGUISH BETWEEN EMPTY WORD AND EMPTY SENTENCE
;; NEED TO FIX DATA SHAPES
;;
;; NEED TO GO THROUGH AND REDO

;;===================================================================
;; IMPORTS
;;===================================================================

(use-modules (srfi srfi-1))

;;===================================================================
;; WF Algebra
;;
;; Consider an expresion like
;; `1 + X + XY`
;; In wfc, this is called a sentence
;;
;; 0 is the empty sentence
;;
;; each summand is a word
;;
;; 1 is the empty word
;;
;; each letter in the word is called a character
;;
;; each character is a symbol
;;
;; the first layer of composition is multiplying two words, which
;; means just taking the union
;;===================================================================

;;===================================================================
;; PRIMITIVES
;;===================================================================

;;-------------------------------------------------------------------
;; PRIMITIVES: WFCHAR
;;-------------------------------------------------------------------

;; DEFINITIONS

(define (wf-char? x)
    "is argument a wf-char (symbol)"
    (symbol? x))



(define (char= c1 c2)
    "two wf characters are equal when they are equal?"
    (equal? c1 c2))


;;-------------------------------------------------------------------
;; PRIMITIVES: WFword
;;-------------------------------------------------------------------

;; DEFINITIONS

(define (word->chars word)
    (cadr word))



(define (wf-word? wd)
    "a word is a list of (w* [set of wfchars])"
    (and (equal? 'w (car wd))
         (list? (cadr wd))
         (every wf-char? (cadr wd))))



(define (word= w1 w2)
    "two words are equal if their chars are equal"
    (lset= char= (word->chars w1) (word->chars w2)))



;; CONSTRUCTORS

(define (char->word char)
   "convert any symbol to a word with that singleton char"
   (chars->word `(,char)))


(define (chars->word chars)
   "convert a list of chars into a word"
   `(w ,chars))


;; one is the empty word
(define word1
    (chars->word '()))



;; OPERATIONS

(define (word* w1 w2)
    "multiply two words (take the union)"
    (chars->word (lset-union char= (word->chars w1) (word->chars w2))))



;;-------------------------------------------------------------------
;; PRIMITIVES: SENTENCES
;;-------------------------------------------------------------------

;; DEFINITIONS

(define (sentence->words se)
    (cadr se))

(define (wf-sentence? se)
    "a sentence is a list of words"
    (and (equal? 's (car se))
         (list? (cadr se))
         (every wf-word? (cadr se))))


(define (sentence= s1 s2)
    "two sentences are equal if their word sets contain equal words"
    (lset= word= (sentence->words s1) (sentence->words s2)))


;; CONSTRUCTORS

(define (words->sentence ws)
    `(s ,ws))

(define sentence0
    (words->sentence '()))

(define (word->sentence w)
    (words->sentence `(,w)))


(define (sentence0? se)
    "is a sentence the zero sentence"
    (equal? sentence0 se))


(define (char->sentence c)
    (word->sentence (char->word c)))

(define sentence1
    (word->sentence word1))


;; OPERATIONS

(define (sentence+ s1 s2)
    "sentence plus = xor of words"
    (define words1 (sentence->words s1))
    (define words2 (sentence->words s2))
    (define new-words (lset-xor word= words1 words2))
    (words->sentence new-words))


(define (wxs-fold w1 s2 accum-s)
    (define (nonempty-case)
        (define words2     (sentence->words s2))
        (define head2-w    (car words2))
        (define tail2-ws   (cdr words2))
        (define tail2-s    (words->sentence tail2-ws))
        (define summand-w  (word* w1 head2-w))
        (define summand-s  (word->sentence summand-w))
        (define new-accum  (sentence+ summand-s accum-s))
        (wxs-fold w1 tail2-s new-accum))
    (if (sentence0? s2)
        accum-s
        (nonempty-case)))


(define (wxs w1 s2)
    "word times sentence"
    (wxs-fold w1 s2 sentence0))

(define (sxs-fold s1 s2 accum)
    (define (nonempty-case)
        (define words1     (sentence->words s1))
        (define head1-w    (car words1))
        (define tail1-ws   (cdr words1))
        (define tail1-s    (words->sentence tail1-ws))
        (define summand-s  (wxs head1-w s2))
        (define new-accum  (sentence+ summand-s accum))
        (sxs-fold tail1-s s2 new-accum))
    (if (sentence0? s1)
        accum
        (nonempty-case)))

(define (sentence* s1 s2)
    "sentence times sentence"
    (sxs-fold s1 s2 sentence0))




;;===================================================================
;; Porcelain functions
;;===================================================================

(define (wf-thing? thing)
    "thing that is a wf thing"
    (or (equal? 0 thing)
        (equal? 1 thing)
        (wf-char? thing)
        (wf-word? thing)
        (wf-sentence? thing)))


(define (coerce-to-sentence wf-thing)
    "coerce any wf-thing to a sentence"
    (cond ((equal? 0 wf-thing)     sentence0)
          ((equal? 1 wf-thing)     sentence1)
          ((wf-char? wf-thing)     (char->sentence wf-thing))
          ((wf-word? wf-thing)     (word->sentence wf-thing))
          ((wf-sentence? wf-thing) wf-thing)))

(define-syntax w+
    (syntax-rules ()
        ((w+)
         sentence0)
        ((w+ expr)
         (coerce-to-sentence expr))
        ((w+ expr rest ...)
         (sentence+ (coerce-to-sentence expr) (w+ rest ...)))))

(define-syntax w*
    (syntax-rules ()
        ((w*)
         sentence1)
        ((w* expr)
         (coerce-to-sentence expr))
        ((w* expr rest ...)
         (sentence* (coerce-to-sentence expr) (w* rest ...)))))

(define (pt se)
    "pretty tree"
    (cons 'w+ (pt-words (sentence->words se))))

(define (pt-words ws)
    (map pt-word ws))

(define (pt-word w)
    (cons 'w* (word->chars w)))

;;===================================================================
;; POLYNUMBER Logic
;;
;; A polynumber is a list of 0s and 1s with a fixed length
;;
;; (pn (arity Arity) BinList)
;;===================================================================

(define (pn-arity polynumber)
    "get the arity of a polynumber"
    ;; hack but fuck you
    (cadr (cadr polynumber)))


(define (pn-components polynumber)
    "get the components of a polynumber"
    ;; hack but fuck you
    (caddr polynumber))


(define (pn-zeros arity)
    "make a zero polynumber of a given arity"
    (define (zeros-list-arity arity)
        "make a list of zeros of length 2^arity"
        (make-list (expt 2 arity) 0))
    `(pn (arity ,arity) ,(zeros-list-arity arity)))


(define (int->binlist-be-acc n acc)
  "convert an integer to a binary list big-endian with accumulator"
  (cond ((equal? n 0) (cons 0 acc))
        ((equal? n 1) (cons 1 acc))
        (else
          ;; js tier hack but IDK the correct way to do this
          ((lambda ()
            (define rem (remainder n 2))
            (define new-n (quotient n 2))
            (define new-acc (cons rem acc))
            (int->binlist-be-acc new-n new-acc))))))

(define (int->binlist-le n)
    "convert an integer to a binary list little-endian"
    (if (<= 0 n)
        (reverse (int->binlist-be-acc n '()))
        (error "n must be HIV-positive")))

(define (int->binlist-le-len n desired-length)
    "convert an integer to a binary list little-endian with a desired length"
    (define naive-binlist (int->binlist-le n))
    (define num-extra-zeros (- desired-length (length naive-binlist)))
    (define extra-zeros (make-list num-extra-zeros 0))
   ;(display `(int->binlist-le-len ,n ,desired-length))
   ;(display "\n")
    (append naive-binlist extra-zeros))


(define (seq min-n max-n)
    (iota (+ 1 (- max-n min-n)) min-n))



(define (binlist-fold slot-flags sentences acc)
   ;(display `(binlist-fold ,sentences ,acc))
   ;(display "\n")
    (cond ((null? slot-flags)
                acc)
          ((equal? 0 (car slot-flags))
                (binlist-fold (cdr slot-flags) (cdr sentences) acc))
          ((equal? 1 (car slot-flags))
                (binlist-fold (cdr slot-flags)
                              (cdr sentences)
                              (sentence* (car sentences) acc)))))


(define (pn-eval-summand this-index0-int polynumber sentences)
    "evaluate a summand"
    ;; if this coefficient is 0, just return sentence 0
    (define components (pn-components polynumber))
    (define this-coefficient (list-ref components this-index0-int))
    ;(display this-coefficient))
    ;(display `(pn-eval-summand ,this-index0-int ,polynumber ,sentences)))
    ;(display "\n"))
    (if (equal? 0 this-coefficient)
        sentence0
        ;; otherwise we have to compute
        ;; for instance if (list representation of)
        ;; the index0 is
        ;; '(1 0 1)
        ;; and the sentences are
        ;; '(x y z)
        ;; then we return x*z
        ;; fold over the binary representation
        ;; taking the product
        ;; convert index0-int to a list of length=arity
        ((lambda()
            (define this-index0-binlist (int->binlist-le-len this-index0-int (pn-arity polynumber)))
           ;(display this-index0-binlist)
            (binlist-fold this-index0-binlist sentences sentence1)))))


(define (pn-eval-index0-ints index0-ints acc polynumber sentences)
    "evaluate a polynumber folding over indexes and an accumulator"
    ;; if we're out of integers then we're done
    ;; return the accumulator
   ;(display `(pn-eval-index0-ints ,index0-ints ,acc ,polynumber ,sentences))
   ;(display "\n")
    (if (null? index0-ints)
        acc
        ;; otherwise
        ;; in this case we figure out this summand,
        ;; add it to the accumulator
        ;; and call ourselves recursively
        ((lambda()
            (define this-index0-int (car index0-ints))
            (define new-index0-ints (cdr index0-ints))
            (define this-summand    (pn-eval-summand this-index0-int polynumber sentences))
            (define new-acc         (sentence+ acc this-summand))
           ;(display new-acc)
           ;(display "\n")
            (pn-eval-index0-ints new-index0-ints new-acc polynumber sentences)))))
            ;(display `(,this-index0-int ,new-index0-ints ,this-summand ,new-acc)))))


(define (pn-eval-safe polynumber sentences)
    ;; for each index i
    ;; convert i to a little-endian list of length=arity
    ;; use those as slot flags in the sentences
    ;; take the sum
    (define min-index0-int 0)
    (define max-index0-int (- (expt 2 (pn-arity polynumber)) 1))
    (define index0-int-sequence (seq min-index0-int max-index0-int))
    (define components (pn-components polynumber))
    ;(display `(pn-eval-safe ,polynumber ,sentences))
    ;(display "\n")
    (pn-eval-index0-ints index0-int-sequence sentence0 polynumber sentences))

(define (pn-eval polynumber sentences)
    "evaluate a polynumber on a given list of sentences"
    ;(display `(pn-eval ,polynumber ,sentences))
    ;(display "\n")
    ;; make sure the arity of the polynumber matches the number of sentences
    (if (equal? (pn-arity polynumber) (length sentences))
        (pn-eval-safe polynumber sentences)
        (error "arity of polynumber does not match number of sentences")))

;; ~~this doesn't work but I'm not sure why~~
;; UPDATE: this seems to work but I'm not sure why

(define (lxor x y)
    (cond ((and (equal? x 0) (equal? y 0)) 0)
          ((and (equal? x 1) (equal? y 0)) 1)
          ((and (equal? x 0) (equal? y 1)) 1)
          ((and (equal? x 1) (equal? y 1)) 0)))


(define (components-plus-foldl components-left components-right acc)
    "add two component lists that are assumed to be the same length"
    (cond
        ;; if the left one is empty, return the accumulator (reversed)
        ((null? components-left)
            (reverse acc))
        ;; otherwise, peel off the first two elements of the lists
        ;; take their xor
        ;; prepend it to the acc
        ;; recurse
        (else
            ((lambda()
                (define this-head-left       (car components-left))
                (define this-head-right      (car components-right))
                (define this-component       (lxor this-head-left this-head-right))
                (define new-components-left  (cdr components-left))
                (define new-components-right (cdr components-right))
                (define new-acc              (cons this-component acc)
                (components-plus-foldl new-components-left
                                       new-components-right
                                       new-acc)))))))

(define (pn-plus pnl pnr)
    (if (not (equal? (pn-arity pnl) (pn-arity pnr)))
        (error "attempt to add polynumbers of different arities")
        `(pn (arity ,(pn-arity pnl))
             ,(components-plus-foldl (pn-components pnl)
                                     (pn-components pnr)
                                     '()))))


(define (bitwise-union-of-bitlists xs ys acc)
    (cond ((empty? xs)
                (reverse acc))
          (else
                ((lambda ()

(define (bitwise-union-of-integers arity x y)
    (binlist-le->int
        (bitwise-union-of-bitlists (int->binlist-le x)
                                   (int->binlist-le y)
                                   '())))


(define (lnot bit)
    (cond ((equal? bit 0) 1)
          ((equal? bit 1) 0)))


(define (we-is-flipping the-list the-idx0 cur-idx0 acc)
    (define cur-bit (car the-list))
    (define new-the-list (cdr the-list))
    (cond
        ;; is this the bit we're supposed to flip
        ((equal? the-idx0 cur-idx0)
            (append (reverse (cons (lnot cur-bit) acc))
                    new-the-list))
        (else
            (we-is-flipping new-the-list the-idx0 (+ 1 cur-idx0) (cons cur-bit acc)))))

(define (we-be-flipping the-list the-idx0)
    (we-is-flipping the-list the-idx0 0 '()))

(define (bitflip components-l
                 components-r
                 arity
                 cur-idx0-l
                 cur-idx0-r
                 target)
    ;; if the product of the left component and right component is 0
    ;;      move along
    ;; if the product of the left component and right component is 1
    ;;      bitflip the accumulator in the position given by
    ;;      the bitwise union of their binary representations
    (define target-idx0 (bitwise-union-of-integers arity cur-idx0-l cur-idx0-r))
    (define cur-component-l (list-ref cur-idx0-l components-l))
    (define cur-component-r (list-ref cur-idx0-r components-r))
    (define cur-bit (lxor cur-component-l cur-component-r))
    (define is-we-flipping (eq? cur-bit 1))
    (if is-we-flipping
        (we-be-flipping target target-idx0)
        target))

(define (components-times-safe components-l
                               components-r
                               arity
                               cur-idx0-l
                               max-idx0-l
                               cur-idx0-r
                               max-idx0-r
                               components-result-acc)
    (cond
        ;; end of iteration on the left
        ;; done
        ((< max-idx0-l cur-idx0-l)
            components-result-acc)
        ;; end of iteration on the right
        ;; reset cur-idx0-r to 0
        ;; increment cur-idx0-l
        ((< max-idx0-r cur-idx0-r)
            ((lambda()
                (define new-cur-idx0-l (+ 1 cur-idx0-l))
                (define new-cur-idx0-r 0)
                (components-times-safe components-l
                                       components-r
                                       arity
                                       new-cur-idx0-l
                                       max-idx0-l
                                       new-cur-idx0-r
                                       max-idx0-r
                                       components-result-acc))))
        ;; general case
        ;;
        ;; cur-idx0-l stays constant
        ;; cur-idx0-r increments by 1
        ;;
        ;; if the product of the left component and right component is 0
        ;;      move along
        ;; if the product of the left component and right component is 1
        ;;      bitflip the accumulator in the position given by
        ;;      the bitwise union of their binary representations
        (else
            ((lambda()
                (define new-cur-idx0-r (+ 1 cur-idx0-r))
                (define new-acc (bitflip components-l
                                         components-r
                                         arity
                                         cur-idx0-l
                                         cur-idx0-r
                                         components-result-acc))
                (components-times-safe components-l
                                       components-r
                                       arity
                                       cur-idx0-l
                                       max-idx0-l
                                       new-cur-idx0-r
                                       max-idx0-r
                                       new-acc))))))

(define (pn-times pnl pnr)
    "multiply two polynumbers"
    (error "not yet implemented"))


(define (pn-apply pn args)
    "apply a polynumber to a list of polynumbers; length of list must match the arity of the polynumber"
    (error "not yet implemented"))

;(define pn-xor `(pn (arity 2) (1 1 1 1)))
;(define sx (coerce-to-sentence 'x))
;(define sy (coerce-to-sentence 'y))
;(display (pt (pn-eval pn-xor `(,sx ,sy))))

